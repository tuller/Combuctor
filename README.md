![Preview](http://jaliborc.com/images/addons/large/combuctor/inventory-beauty.jpg)

[![Install](http://jaliborc.com/images/external/twitch_client.png)](https://www.curseforge.com/wow/addons/combuctor/download?client=y) [![Patreon](http://jaliborc.com/images/external/patreon.png#1)](https://www.patreon.com/user?u=9248226) [![Wiki](http://jaliborc.com/images/external/github_wiki.png)](https://github.com/tullamods/Wildpants/wiki)

# Combuctor :handbag:
Combuctor is a bag replacement addon designed to help the player find items as quickly and as easily as possible, while still feeling part of the default interface. Beyond the basic all-bags-in-one functionality, Combuctor provides features such as:
* Ability to view the items of any character and guild, from anywhere
* Inventory, bank, vault and guild bank support
* Coloring based on item quality and more
* Intelligent item search engine
* Tooltip item counts
* Item rulesets
* Databroker support
