## Interface: 80000
## Title: |cff5FC412Combuctor|r
## Author: Tuller & Jaliborc (João Cardoso)
## Notes: Pants are a dangerous foe
## SavedVariables: Combuctor_Sets

## OptionalDeps: BagBrother, WoWUnit
## Version: 8.0

main.lua

common\Wildpants.xml
components\components.xml
style.xml

legacy.lua
